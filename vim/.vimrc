runtime bundle/vim-pathogen/autoload/pathogen.vim
set nocompatible
set termguicolors
let &t_8f = "\e[38;2;%lu;%lu;%lum"
let &t_8b = "\e[48;2;%lu;%lu;%lum"

execute pathogen#infect()
"syntax on
filetype plugin indent on
" show existing tab with 2 spaces width
set tabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
" On pressing tab, insert 2 spaces
set expandtab

map <F2> :NERDTreeToggle<CR>

" VimWiki settings
let wiki_1 = {}
let wiki_1.path = '~/git/public/gitlab-encephalon/vimwiki/markdown/'
let wiki_1.syntax = 'markdown'
let wiki_1.ext = '.md'

let wiki_2 = {}
let wiki_2.path = '~/git/public/gitlab-encephalon/vimwiki/core/'
"let wiki_2.path_html = '~/public_html/'
let wiki_2.syntax = 'markdown'
let wiki_2.ext = '.md'
"let wiki_2.nested_syntaxes = {'bash': 'bash'}

let g:vimwiki_list = [wiki_1, wiki_2]

function! VimwikiLinkHandler(link)
  " Use Vim to open external files with the 'vfile:' scheme.  E.g.:
  "   1) [[vfile:~/Code/PythonProject/abc123.py]]
  "   2) [[vfile:./|Wiki Home]]
  let link = a:link
  if link =~# '^vfile:'
    let link = link[1:]
  else
    return 0
  endif
  let link_infos = vimwiki#base#resolve_link(link)
  if link_infos.filename == ''
    echomsg 'Vimwiki Error: Unable to resolve link!'
    return 0
  else
    exe 'tabnew ' . fnameescape(link_infos.filename)
    return 1
  endif
endfunction

" Copy/Paste to system clipboard
noremap <Leader>y "*y
noremap <Leader>p "*p
noremap <Leader>Y "+y
noremap <Leader>P "+p

" see https://jeffkreeftmeijer.com/vim-number/
"set number relativenumber
"augroup numbertoggle
"  autocmd!
"  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
"  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
"augroup END

" Solarized dark colour scheme
syntax enable
set background=dark
colorscheme solarized

" omnicomplete
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" Highlight search results
set hlsearch

" Insert timestamp http://vim.wikia.com/wiki/Insert_current_date_or_time
:nnoremap <F5> "=strftime("%c")<CR>P
:inoremap <F5> <C-R>=strftime("%c")<CR>

" lightline.vim
" https://github.com/itchyny/lightline.vim
set laststatus=2
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ }

" numbers
"let g:numbers_exclude = ['Calendar', 'nerdtree']
set nonumber
set norelativenumber

nnoremap <F3> :NumbersToggle<CR>
nnoremap <F4> :NumbersOnOff<CR>

" git-gutter
set updatetime=100

" Todo.txt-vim
" Use todo#complete as the omni complete function for todo files
au filetype todo setlocal omnifunc=todo#Complete

" Auto complete projects
au filetype todo imap <buffer> + +<C-X><C-O>

" Auto complete contexts
au filetype todo imap <buffer> @ @<C-X><C-O>
