Dotfiles
========

## How to use

Clone this repo then,
```
git submodule init
git submodule update
```

Use GNU Stow to deploy these files

```
sudo pacman -S stow
```

### Move files into this repo
```
mv ~/.config/example ~/git/dotfiles/stow_pkg/.config/example
```

### Stow the files
```
cd ~/git/dotfiles
stow -t ~ stow_pkg
```
