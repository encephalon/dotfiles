# list hosts using `vagrant global-status`
# replace <hostid> and ports
ssh $(vagrant ssh-config hostid | awk 'NR>1 {print " -o "$1"="$2}') -L 8080:localhost:8080 localhost
