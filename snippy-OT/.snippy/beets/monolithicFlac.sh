outdir="$(basename "${PWD}")"
mkdir -p "${outdir}"
for i in *.flac; do
  shnsplit -f ${i}.cue -t "%n - %t" -o flac -d "${outdir}" ${i}
  rm -f "${outdir}/00 - pregap.flac"
  cp -r Artwork/* "${outdir}"
  cd "${outdir}"
  cuetag.sh "../${i}.cue" *.flac 2>/dev/null
done
