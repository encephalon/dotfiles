while true
do
  clear
  for i in `redis-cli --scan`
  do
    printf "%20s: " $i
    redis-cli llen $i
  done | awk 'BEGIN{c=0}{c=c+$NF; print}END{printf("\n%20s: %s","Total",c)}'
  sleep 2
done
