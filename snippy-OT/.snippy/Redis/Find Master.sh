sdc-listmachines | json -C "/$VFILTER-redis/i.exec(this.name)" -a ips[0] | \
xargs -P 0 -I{} ssh root@{} \
'redis-cli info replication | grep "role:master" >/dev/null && echo `uname -n`' 2>/dev/null
