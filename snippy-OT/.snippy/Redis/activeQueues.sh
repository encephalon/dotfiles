while true
do
  clear
  for i in $(redis-cli keys q:jobs:\*:active | sort)
  do
    printf "%50s: " $i
    redis-cli zcard $i
  done | awk 'BEGIN{c=0}{c=c+$NF; print}END{printf("\n%50s: %s","Total",c)}'
  sleep 1
done