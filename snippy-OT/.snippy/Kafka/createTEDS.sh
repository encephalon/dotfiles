/usr/local/kafka/kafka_2.11-0.9.0.1/bin/kafka-topics.sh --create --partitions 2 --replication-factor 3 --zookeeper $zookeepers --topic ${ENV:?Set ENV first}-teds-replication-done
/usr/local/kafka/kafka_2.11-0.9.0.1/bin/kafka-topics.sh --create --partitions 2 --replication-factor 3 --zookeeper $zookeepers --topic ${ENV:?Set ENV first}-teds-replication-start
