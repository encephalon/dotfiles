kstat -pc zone_memory_cap :::rss :::physcap | \
awk '
{
  used=$NF
  getline
  total=$NF
  pfree=(total-used)/total*100
  usedmb=used/1024/1024
  totalmb=total/1024/1024
  usedgb=usedmb/1024
  totalgb=totalmb/1024
}
END{
  printf "%s %i / %s %i\n", "Used (MB)",usedmb,"Total (MB)",totalmb
  printf "%s%i%s\n", "Percent Free ",pfree,"%"
}'
