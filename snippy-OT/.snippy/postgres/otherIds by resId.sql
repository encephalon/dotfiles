SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_0.version
INNER JOIN nimbus_0.resource ON nimbus_0.version."resourceId" = nimbus_0.resource.id
WHERE nimbus_0.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_1.version
INNER JOIN nimbus_1.resource ON nimbus_1.version."resourceId" = nimbus_1.resource.id
WHERE nimbus_1.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_2.version
INNER JOIN nimbus_2.resource ON nimbus_2.version."resourceId" = nimbus_2.resource.id
WHERE nimbus_2.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_3.version
INNER JOIN nimbus_3.resource ON nimbus_3.version."resourceId" = nimbus_3.resource.id
WHERE nimbus_3.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_4.version
INNER JOIN nimbus_4.resource ON nimbus_4.version."resourceId" = nimbus_4.resource.id
WHERE nimbus_4.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_5.version
INNER JOIN nimbus_5.resource ON nimbus_5.version."resourceId" = nimbus_5.resource.id
WHERE nimbus_5.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_6.version
INNER JOIN nimbus_6.resource ON nimbus_6.version."resourceId" = nimbus_6.resource.id
WHERE nimbus_6.resource.id in (${RESID})
UNION
SELECT version."fileId",resource."thumbnailId",version."renditionId" FROM nimbus_7.version
INNER JOIN nimbus_7.resource ON nimbus_7.version."resourceId" = nimbus_7.resource.id
WHERE nimbus_7.resource.id in (${RESID});