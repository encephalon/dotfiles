\pset pager on
SELECT * FROM (
SELECT 0 AS shard,* FROM nimbus_0."auditRecord" UNION ALL
SELECT 1,* FROM nimbus_1."auditRecord" UNION ALL
SELECT 2,* FROM nimbus_2."auditRecord" UNION ALL
SELECT 3,* FROM nimbus_3."auditRecord" UNION ALL
SELECT 4,* FROM nimbus_4."auditRecord" UNION ALL
SELECT 5,* FROM nimbus_5."auditRecord" UNION ALL
SELECT 6,* FROM nimbus_6."auditRecord" UNION ALL
SELECT 7,* FROM nimbus_7."auditRecord"
) AS tmp
WHERE "userId" = 116617694412804332
ORDER BY "createdAt";
