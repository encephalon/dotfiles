SELECT shard, id, email, "tenantId", "createdAt","isConfirmed" FROM (
SELECT *, 0 as shard FROM nimbus_0.v_principal UNION ALL
SELECT *, 1 as shard FROM nimbus_1.v_principal UNION ALL
SELECT *, 2 as shard FROM nimbus_2.v_principal UNION ALL
SELECT *, 3 as shard FROM nimbus_3.v_principal UNION ALL
SELECT *, 4 as shard FROM nimbus_4.v_principal UNION ALL
SELECT *, 5 as shard FROM nimbus_5.v_principal UNION ALL
SELECT *, 6 as shard FROM nimbus_6.v_principal UNION ALL
SELECT *, 7 as shard FROM nimbus_7.v_principal
) as tbl WHERE email LIKE 'mthornba%';
