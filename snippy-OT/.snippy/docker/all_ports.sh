docker ps -q | xargs -n1 docker inspect --format='{{.Name | printf "%-15s"}}{{range $p, $conf := .NetworkSettings.Ports}} {{$p}} -> {{(index $conf 0).HostPort}} {{end}}'
