[[ $(whoami) == "root" ]] && COLOR="01;31m" || COLOR="01;32m"
PS1="\[\e]0;\u@\h: \w\a\]\${debian_chroot:+(\$debian_chroot)}\[\033[${COLOR}\]\u@`mdata-get domainName 2>/dev/null`\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ "
