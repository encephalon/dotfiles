for env in dev pl pt public01al3 qa release sit; do
  for user in nimbus_public nimbus_private coredump_public coredump_private pg_private pg_public; do
    knife data bag show ${env} secrets -Fj | json -a manta.${user} > id_rsa.${env}.${user}
  done
done