mget ~~/reports/usage/storage/latest | json -a storage.stor.bytes | awk '{printf("%.2f", $1/1024/1024/1024); print "GB " $2}'
