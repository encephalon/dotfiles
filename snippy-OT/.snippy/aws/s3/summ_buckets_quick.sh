aws s3api --endpoint-url $S3_ENDPOINT --profile $AWSPROFILE list-buckets --query 'Buckets[].Name'  --output text \
| xargs -n1 -I{} sh -c \
"echo {}:; aws s3api list-objects --endpoint-url $S3_ENDPOINT --profile $AWSPROFILE --query "[sum(Contents[].Size), length(Contents[])]" --output text --bucket {}"
