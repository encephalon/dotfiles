sdc-listmachines | json -C "/$VFILTER-app00[2-9]/i.exec(this.name)" -a ips[0] | \
xargs -l -I {} \
ssh -o stricthostkeychecking=no root@{} \
'nohup chef-client -c /opt/local/chef/client.rb >/tmp/chef-client.out 2>&1 &'