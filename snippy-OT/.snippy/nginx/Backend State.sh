awk '
BEGIN{
  a="api_core"
  b="socketio"
  printf "%8s\t%8s\n",a,b
}{
  printf "%-8s\t",$0;
}{
  getline < x;
}{
  printf "%-8s\n",$0;
}' x=<(curl -s "localhost:49152/status" | json -a upstreams.socketio.peers | json -a id state ) <(curl -s "localhost:49152/status" | json -a upstreams.api_core.peers | json -a id state)
