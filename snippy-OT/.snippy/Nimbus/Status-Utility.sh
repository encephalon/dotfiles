sdc-listmachines | json -C "/$VFILTER-utility/i.exec(this.name)" -a ips[0] | \
xargs -P 0 -l -I {} \
ssh -o stricthostkeychecking=no {} \
'printf "%s\t%s\t%s\n" "$(uname -n)" "$(sudo service nimbus-app status | (read string; echo -n "$string "; ps -p $(echo -n $string | grep -oe "[0-9]*") -ho etime 2>/dev/null))" "$(grep version /opt/local/nimbus/package.json)"' \
| sort
