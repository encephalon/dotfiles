sdc-listmachines | json -C "/$VFILTER-app[0-9]/i.exec(this.name)" -a ips[0] | \
xargs -l -I {} \
ssh -o stricthostkeychecking=no root@{} \
'printf "%s\t%s\n" `uname -n` "`svcadm restart nimbus-app`"'