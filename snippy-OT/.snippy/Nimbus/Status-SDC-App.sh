sdc-listmachines | json -C "/$VFILTER-app[0-9]/i.exec(this.name)" -a ips[0] | \
xargs -P 0 -l -I {} \
ssh -o stricthostkeychecking=no root@{} \
'printf "%s\t%s\t%s\n" "`uname -n`" "`svcs -Ho svc\,stime\,state nimbus-app`" "`grep version /opt/local/nimbus/package.json`"' \
| sort
