sdc-listmachines | json -C "/$VFILTER-app-portal/i.exec(this.name)" -a ips[0] | \
xargs -l -i{} ssh -o stricthostkeychecking=no root@{} \
'printf "%s\t%s\t%s\n" "`uname -n`" "`svcs -aHo svc\,stime\,state | egrep -i \"nimbus|portal\"`" "`grep version /opt/local/nimbusPortal/package.json`"'