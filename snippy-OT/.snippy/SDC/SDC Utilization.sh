./querySDC $DC /servers?page=1\&perPage=100000 -C "/$PFILTER/i.exec(this.hostname)" \
-a hostname memory_total_bytes memory_available_bytes memory_provisionable_bytes  | \
sort | awk '
BEGIN{
hfmt="%-20s%15s%15s\n"
dfmt="%-20s%15.1f%15.1f\n"
printf hfmt, "Hostname", "Avail_Memory", "Percent_Used"
}
{
cn=$1; memtot=$2; memavail=$3; memprov=$4
printf dfmt, cn, memprov/1073741824, (memtot-memprov)/memtot*100
}'
