cd /home/ubuntu/deploy
for UUID in $(./querySDC $DC /servers?page=1\&perPage=100000 -C "/$PFILTER/i.exec(this.hostname)" -a uuid); do
  VMS=$(./querySDC $DC /vms?page=1\&perPage=100000\&state=running -C "/${UUID}/.exec(this.server_uuid)" -C "/$VFILTER/i.exec(this.alias)" -a alias 2>/dev/null)
  if [ "$VMS" != "" ]; then
    printf "\n%s:\n" $(./querySDC $DC /servers/$UUID -a hostname 2>/dev/null)
    printf "\t%s\n" $VMS
  fi
done
