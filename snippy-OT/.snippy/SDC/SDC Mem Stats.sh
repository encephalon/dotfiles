./querySDC $DC /servers?page=1\&perPage=100000 -C "/$PFILTER/i.exec(this.hostname)" \
-a hostname memory_total_bytes memory_available_bytes memory_provisionable_bytes memory_arc_bytes | \
sort | awk '
BEGIN{
hfmt="%-20s%15s%15s%15s%15s\n"
dfmt="%-20s%15i%15i%15.0f%15.0f\n"
printf hfmt, "Hostname", "Mem_Total", "Mem_Avail", "Mem_Prov", "Mem_Arc"
}
{
cn=$1; memtot=$2; memavail=$3; memprov=$4; memarc=$5
printf dfmt, cn, memtot, memavail, memprov, memarc
}'
