sdc-listmachines | json -C "/$VFILTER-app[0-9]/i.exec(this.name)" -a ips[0] | \
xargs -l -P 0 -I {} \
ssh -o stricthostkeychecking=no root@{} \
'printf "%s:\n%s\n" `uname -n` "`:`"'