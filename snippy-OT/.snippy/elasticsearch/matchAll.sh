# also removed 'pretty' for increased performance
# match_all is the default, so this:
# curl -XPOST "http://localhost:9200/core_index_production/_search?pretty" -d '{ "size": 0, "filter": { "match_all": {} } }'
# is equivalent to this:
curl -XPOST "http://localhost:9200/core_index_production/_search" -d '{ "size": 0 }'