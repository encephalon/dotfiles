curl -XPUT "http://localhost:9200/core_index_production_$(date +%Y%m%d)" -d '
{
  "settings" : {
    "index" : {
      "number_of_shards" : 5,
      "number_of_replicas" : 1
    }
  }
}'