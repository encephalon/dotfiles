curl -sXPOST "http://localhost:9200/_all/_search?pretty&filter_path=aggregations" -d'{
    "_source": false,
    "aggs": {
        "index": {
            "terms": { "field": "_index" },
            "aggs": {
                "types": {
                    "terms": { "field": "_type" }
                }
            }
        }
    }
}'