export EDITOR=vim
export XDG_CONFIG_HOME="$HOME/.config"
export KUBECONFIG="$HOME/.kube/config:$HOME/.kube/config.bp2"
export ZGEN_RESET_ON_CHANGE=(${HOME}/.zshrc)
export BUKU_COLORS=FCexd
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE=fg=10

## POWERLEVEL9K SETTINGS ##
POWERLEVEL9K_MODE='nerdfont-complete'
POWERLEVEL9K_SSH_ICON="\uf023 "
#POWERLEVEL9K_HOST_ICON="\uf303 "
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(icons_test)
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(command_execution_time host vi_mode dir dir_writable rbenv)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status vcs root_indicator background_jobs time)
#POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=2
#ZSH_THEME="powerlevel9k/powerlevel9k"
#POWERLEVEL9K_STATUS_VERBOSE=false
#POWERLEVEL9K_STATUS_OK_IN_NON_VERBOSE=true
#POWERLEVEL9K_PROMPT_ON_NEWLINE=true
#POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=''
#POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%K{white}%F{black} \UE12E `date +%T` %f%k%F{white}%f "
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
#POWERLEVEL9K_CUSTOM_INTERNET_SIGNAL="zsh_internet_signal"
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon  dir vcs)
##POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_internet_signal os_icon  dir vcs)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status)
#POWERLEVEL9K_OS_ICON_BACKGROUND="white"
#POWERLEVEL9K_OS_ICON_FOREGROUND="blue"
POWERLEVEL9K_DIR_HOME_FOREGROUND="255"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="255"
#POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"
POWERLEVEL9K_TIME_BACKGROUND="white"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="255"
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND="255"
# Vi-Mode
POWERLEVEL9K_VI_MODE_INSERT_BACKGROUND='154'
POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND='022'
POWERLEVEL9K_VI_MODE_NORMAL_BACKGROUND='255'
POWERLEVEL9K_VI_MODE_NORMAL_FOREGROUND='008'
##

# Load keys into ssh-agent
eval $(keychain --quiet --eval id_rsa id_rsa.github id_rsa.gitlab id_rsa.lab1 id_rsa.sdcmirror id_rsa.sdcroot)

# Aliases
alias viz="berks viz -f dot -o graph.dot && dot2ascii -f graph.dot && rm graph.dot"
alias ipa='ip -4 -o addr | egrep -o "([0-9]{1,3}\.){3}[0-9]{1,3}"'
alias catimg='img2sixel -p 16 -h 128 '
alias tf=terraform
alias wiki='vim -c VimwikiIndex'
alias diary='vim -c "exe \"normal\" 2 . \"\\\\wi\""'
alias fuku='BROWSER=firefox buku -o'
alias b='buku --np'

# Functions
function csdiff () { sdiff -w $(stty size | awk '{print $NF}') $@ | colordiff; }
function rolediff () { csdiff <(knife role show ${1%.json} -Fj) ${1} }
function envdiff () { csdiff <(knife environment show ${1%.json} -Fj) ${1} }
function dbdiff () { csdiff <(knife data bag show $1 env -Fj) ${1}/env.json | less }
# Strip final EOL from a file
function chompeol () { printf %s "$(cat $1)" > $1 }

PATH=~/.local/bin:$PATH

eval "$(thefuck --alias fuck)"

# The following lines were added by compinstall
#zstyle :compinstall filename '/home/mthornba/.zshrc'
#
#autoload -Uz compinit
#compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
# End of lines configured by zsh-newuser-install

source /home/mthornba/git/.zgen/zgen.zsh
    if ! zgen saved; then
      # specify plugins here
      zgen oh-my-zsh
#      zgen oh-my-zsh plugins/git
      zgen oh-my-zsh plugins/knife

#      zgen load zsh-users/zsh-syntax-highlighting
      zgen load zdharma/fast-syntax-highlighting
      zgen load zsh-users/zsh-history-substring-search
      zgen load zsh-users/zsh-autosuggestions
      zgen load berkshelf/berkshelf-zsh-plugin
      zgen load pelletiermaxime/test-kitchen-zsh-plugin
      zgen load mdumitru/git-aliases
      zgen load marzocchi/zsh-notify
      zgen load joel-porquet/zsh-dircolors-solarized
      zgen load zdharma/zsh-diff-so-fancy
      zgen load jimeh/zsh-peco-history

      # themes
#      zgen load bhilburn/powerlevel9k powerlevel9k
      zgen load romkatv/powerlevel10k powerlevel10k

      # generate the init script from plugins above
      zgen save
    fi

source /home/mthornba/git/tmuxinator/completion/tmuxinator.zsh

if [ -n "$XTERM_VERSION" ]; then
  export TERM=xterm-256color
  compton-trans -w "$WINDOWID" 95 >/dev/null
fi

# Set ruby paths from ChefDK
# eval "$(chef shell-init zsh)"

# Go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/usr/local/go/bin

# set dircolors
#eval `dircolors /home/mthornba/git/public/dircolors-solarized/dircolors.256dark`

# k8s & helm completion
source <(kubectl completion zsh)
#source <(helm completion zsh)
# https://github.com/helm/helm/issues/5046#issuecomment-463576351
source <(helm completion zsh | sed -E 's/\["(.+)"\]/\[\1\]/g')
source <(minikube completion zsh)

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

bindkey -v
#bindkey -M vicmd 'k' history-substring-search-up
#bindkey -M vicmd 'j' history-substring-search-down
bindkey -M vicmd '^[OA' history-substring-search-up
bindkey -M vicmd '^[OB' history-substring-search-down
bindkey -M viins '^[OA' history-substring-search-up
bindkey -M viins '^[OB' history-substring-search-down
